# This file is licensed CC0 by Andrew Harvey <andrew@beyondtracks.com>
#
# To the extent possible under law, the person who associated CC0
# with this work has waived all copyright and related or neighboring
# rights to this work.
# http://creativecommons.org/publicdomain/zero/1.0/

lint:
	xmllint --noout walks/*/*.osm
	# created_by tags shouldn't be present
	! grep "k='created_by'" walks/*/*.osm

setUploadFalse:
	sed -i "s/<osm version='0.6' generator='JOSM'>/<osm version='0.6' upload='false' generator='JOSM'>/" walks/*/*.osm

test:
	./test.sh

lint_clean:
	sed -i "s/\s*<tag k='created_by' v='[^']*' \/>\s*//g" walks/*/*.osm
	sed -i '/^$$/d' walks/*/*.osm
