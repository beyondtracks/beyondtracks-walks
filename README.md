# beyondtracks-walks

This repository contains the source files defining walks on [www.beyondtracks.com](https://www.beyondtracks.com).

They are created in [JOSM](https://josm.openstreetmap.de/) partially derived from OpenStreetMap data. Around 95% of the geometries were copied from OpenStreetMap data the others are from personal GPS track recordings. In some cases, names were also from OpenStreetMap but mostly they are self sourced.

New walks Australia wide are welcome for contribution, but we reserve final editorial decisions on what's included on [www.beyondtracks.com](https://www.beyondtracks.com). 


## Schema
Each walk on beyondtracks is defined in a .osm file within the top level country code directories. Each walk file consists of at least one way tagged `role=primary`, optionally including other ways and nodes.

### Ways
* **name** For way tagged `role=primary`, either a commonly known name for the walk or start/end points including a notable waypoint if it's necessary to avoid ambiguity. For other ways, a commonly known name or description for the sidetrack or alternate route.
* **park** Name of the CAPAD protected area which most of the walk falls in. Walks which span multiple parks aren't well supported so for the time being only one can be listed. In the future this may be removed by an automated process. (only on way tagged `role=primary`)
* **photo** A photo which best represents the walk segment. eg. [`136319147@N08/29755427297`](https://www.flickr.com/photos/136319147@N08/29755427297/) (Flickr Owner / Flickr Photo ID)
* **style** `oneway` | `return` | `loop` Value depending on if the walk starts and ends at the same point without backtracking (loop) or not (oneway) or starts and ends at the same point with backtrack the route (return).
* **role** `primary` | `sidetrack` | `altroute` | `transit_connection` | `2wd_connection` The role of this way. A single `primary` way must exists, optionally additional sidetracks, alternative routes, ways for getting to the start of the walk from a nearby public transit stop, or ways to reach the start from a 2WD car can exist.
* **transport** `carshuffle` | `car` | `foot` | `train` | `bus` | `ferry` | `taxi` | `chairlift` What methods of transport are viable ways of getting to/from the start/end of the walk. Multiple options are separated by a `;`, where multiple options are required together use a `+`. eg. `car;train+bus` means you can either take a car or take a train and bus. Car shuffle is when the end of the walk is away from the start and you need to shuffle multiple cars to complete the trip. (only on way tagged `role=primary`)
* **notice** Optional important notice about the walk, displayed prominetly to users.

### Nodes
* **warn** `hazard` | `information` | `navigational` | `rivercrossing` Used to flag a warning at a point along the track.
* **note** Description of the warning.

## License
Contains information (c) OpenStreetMap contributors made available under the [Open Database License (ODbL)](http://opendatacommons.org/licenses/odbl/1.0/).

As this beyondtracks walks database is a derivative database of OpenStreetMap it is licensed under the [Open Database License (ODbL)](http://opendatacommons.org/licenses/odbl/1.0/).

When using our walks database in other works, please provide attribution, for example: "Contains data from [beyondtracks.com](https://www.beyondtracks.com)."

## Renames

Sometimes we rename walks, in these cases we still want a 302 redirect to occur for people who visit a URL based on the original walk name. A CSV mapping for these renames based on the `park_name/walk_name` are maintained in the renames directory.
