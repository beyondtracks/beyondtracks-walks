#!/bin/dash

# This file is licensed CC0 by Andrew Harvey <andrew@beyondtracks.com>
#
# To the extent possible under law, the person who associated CC0
# with this work has waived all copyright and related or neighboring
# rights to this work.
# http://creativecommons.org/publicdomain/zero/1.0/

OK='\033[0;32mOK\033[0m'
FAIL='\033[0;31mFAIL\033[0m'

NUM_PASSED=0
NUM_FAILED=0

for f in walks/*/*.osm ; do
    echo "$f"

    echo -n "osm should be set to upload=false: "
    `grep '<osm' "$f" | grep "upload='false'" > /dev/null`
    if [ $? -eq 0 ] ; then
        echo $OK
        NUM_PASSED=$(( $NUM_PASSED + 1 ))
    else
        echo $FAIL
        NUM_FAILED=$(( $NUM_FAILED + 1 ))
    fi

    echo -n "osm file should not have bounds: "
    `grep '<bounds' "$f" > /dev/null`
    if [ $? -ne 0 ] ; then
        echo $OK
        NUM_PASSED=$(( $NUM_PASSED + 1 ))
    else
        echo $FAIL
        NUM_FAILED=$(( $NUM_FAILED + 1 ))
    fi

    echo -n "walk filename must start with a number follow by a dash: "
    `basename "$f" | cut -d'-' -f1 | grep -E '^[0-9]+$' > /dev/null`
    if [ $? -eq 0 ] ; then
        echo $OK
        NUM_PASSED=$(( $NUM_PASSED + 1 ))
    else
        echo $FAIL
        NUM_FAILED=$(( $NUM_FAILED + 1 ))
    fi

    PRIMARY=`osmium tags-filter "$f" --omit-referenced w/role=primary --output-format debug --no-progress`
    echo -n "each walk must have a role=primary: "
    if [ `echo "$PRIMARY" | grep '"role"\s*=\s*"primary"' | wc -l` -eq 1 ] ; then
        echo $OK
        NUM_PASSED=$(( $NUM_PASSED + 1 ))
    else
        echo $FAIL
        NUM_FAILED=$(( $NUM_FAILED + 1 ))
    fi

    echo -n "each primary role must have a non-empty name tag: "
    `echo "$PRIMARY" | grep -E '"name"\s*=\s*"[^"]+"' > /dev/null`
    if [ $? -eq 0 ] ; then
        echo $OK
        NUM_PASSED=$(( $NUM_PASSED + 1 ))
    else
        echo $FAIL
        NUM_FAILED=$(( $NUM_FAILED + 1 ))
    fi

    echo -n "each primary role must have a style tag: "
    `echo "$PRIMARY" | grep -E '"style"\s*=\s*"[^"]+"' > /dev/null`
    if [ $? -eq 0 ] ; then
        echo $OK
        NUM_PASSED=$(( $NUM_PASSED + 1 ))
    else
        echo $FAIL
        NUM_FAILED=$(( $NUM_FAILED + 1 ))
    fi

    echo -n "each primary role must have a transport tag: "
    `echo "$PRIMARY" | grep -E '"transport"\s*=\s*"[^"]*"' > /dev/null`
    if [ $? -eq 0 ] ; then
        echo $OK
        NUM_PASSED=$(( $NUM_PASSED + 1 ))
    else
        echo $FAIL
        NUM_FAILED=$(( $NUM_FAILED + 1 ))
    fi

    echo -n "only known tag keys: "
    badkeys=`osmfilter "$f" --out-key 2> /dev/null | cut -f2 | grep --fixed-strings --file=tag-keys.txt --invert-match --line-regexp`
    if [ $? -eq 1 ] ; then
        echo $OK
        NUM_PASSED=$(( $NUM_PASSED + 1 ))
    else
        echo $FAIL
        NUM_FAILED=$(( $NUM_FAILED + 1 ))
        echo $badkeys
    fi

    echo ""
done

echo "$NUM_PASSED tests passed"

if [ $NUM_FAILED -gt 0 ] ; then
    echo '\033[0;31m' "$NUM_FAILED tests failed" '\033[0m'
    exit 1
else
    echo '\033[0;32m' "$NUM_FAILED tests failed" '\033[0m'
    exit 0
fi
